# login-authentication-with-fastapi-and-react



## Getting started

```
uvicorn app.main:app --reload --port 8000
alembic revision --autogenerate
alembic upgrade head
```

## Docs

```
https://github.com/pydantic/pydantic/issues/511
https://python.tutorialink.com/pydantic-sqlalchemy-how-to-work-with-enums/
```
